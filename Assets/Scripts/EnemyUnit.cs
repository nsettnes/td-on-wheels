﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyUnit : MovingUnit
{
    public override void Deselect()
    {
        base.Deselect();
    }

    public override void Select()
    {
        base.Select();
    }

    protected override void SelfAwake_MovingUnit()
    {

    }

    protected override void SelfStart_MovingUnit()
    {

    }
}
