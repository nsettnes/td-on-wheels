﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpawnableBase : MonoBehaviour
{
    public abstract void Spawn();
}
