﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDoBake : SpawnableBase
{
    public override void Spawn()
    {
        NavigationBaker.Instance.Bake();
    }
}
