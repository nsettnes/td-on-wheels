﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHouseComponent : SpawnableBase
{
    public override void Spawn()
    {
        HouseUnit hu = this.GetComponent<HouseUnit>();
        if (hu == null)
            Debug.LogError("I dont have a house unit?");
    }
}
