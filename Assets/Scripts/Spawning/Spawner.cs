﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    #region singleton
    static Spawner _instance;
    public static Spawner Instance
    {
        get
        {
            if (_instance == null)
                _instance = (Spawner)GameObject.FindObjectOfType(typeof(Spawner));

            return _instance;
        }
    }
    #endregion singleton

    public GameObject ObstructionPrefab;
    public GameObject PlanePrefab;
    public GameObject HousePrefab;

    bool isSpawning = false;
    bool spawnUnitNow = false;

    private GameObject objToSpawn;
    private Quaternion lastRotation = Quaternion.identity;

    IEnumerator spawnRoutine;

    public void BeginSpawn(UIS_Spawn.SPAWNTYPE spawntype)
    {
        Debug.Log("Begin spawn");
        if (isSpawning)
        {
            CancelSpawn();
            return;
        }

        if (spawnRoutine != null)
            StopCoroutine(spawnRoutine);
        
        switch(spawntype)
        {
            case UIS_Spawn.SPAWNTYPE.Obstruction:
                spawnRoutine = CR_SpawnObstruction();
                break;
            case UIS_Spawn.SPAWNTYPE.Plane:
                spawnRoutine = CR_SpawnPlane();
                break;
            case UIS_Spawn.SPAWNTYPE.House:
                spawnRoutine = CR_SpawnHouse();
                break;
        }
        isSpawning = true;
        StartCoroutine(spawnRoutine);
    }

    public void EndSpawn()
    {
        Debug.Log("End spawn");
        isSpawning = false;
        spawnUnitNow = true;
    }

    public void CancelSpawn()
    {
        Debug.Log("Cancel spawn");
        isSpawning = false;
        spawnUnitNow = false;
    }

    public void RotateSpawn()
    {
        if(objToSpawn != null)
        {
            objToSpawn.transform.Rotate(Vector3.up, 90f);
        }
    }

    IEnumerator CR_SpawnObstruction()
    {
        Vector3 spawnPos = Vector3.zero;
        objToSpawn = GameObject.Instantiate(ObstructionPrefab, spawnPos, lastRotation);
        objToSpawn.SetActive(false);
        Ray ray;
        RaycastHit hitInfo;
        while (isSpawning)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("Walkable")))
            {
                objToSpawn.SetActive(true);
                spawnPos = hitInfo.point;
                spawnPos.x = Mathf.Round(spawnPos.x / 1f) * 1;
                spawnPos.z = Mathf.Round(spawnPos.z / 1f) * 1;
                objToSpawn.transform.position = spawnPos;
            }
            else
            {
                objToSpawn.SetActive(false);
            }
            yield return null;
        }

        if (spawnUnitNow)
        {
            Spawn(ObstructionPrefab, spawnPos);
        }
        GameObject.Destroy(objToSpawn);
        spawnUnitNow = false;
        yield return null;
    }

    IEnumerator CR_SpawnPlane()
    {
        Vector3 spawnPos = Vector3.zero;
        objToSpawn = GameObject.Instantiate(PlanePrefab, spawnPos, lastRotation);
        objToSpawn.SetActive(false);
        Ray ray;
        RaycastHit hitInfo;
        while (isSpawning)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("UnderLayer")))
            {
                objToSpawn.SetActive(true);
                //find out where the plane can fit into existing planes
                spawnPos = hitInfo.point;
                spawnPos.x = Mathf.Round(spawnPos.x / 10f) * 10;
                spawnPos.z = Mathf.Round(spawnPos.z / 10f) * 10;
                objToSpawn.transform.position = spawnPos;
            }
            else
            {
                objToSpawn.SetActive(false);
            }
            yield return null;
        }

        if (spawnUnitNow & spawnPos != Vector3.zero)
        {
            Spawn(PlanePrefab, spawnPos);
        }
        GameObject.Destroy(objToSpawn);
        spawnUnitNow = false;
        yield return null;
    }


    IEnumerator CR_SpawnHouse()
    {
        Vector3 spawnPos = Vector3.zero;
        objToSpawn = GameObject.Instantiate(HousePrefab, spawnPos, lastRotation);
        objToSpawn.GetComponent<Unit>().enabled = false;
        objToSpawn.SetActive(false);
        Ray ray;
        RaycastHit hitInfo;
        while (isSpawning)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("Walkable")))
            {
                objToSpawn.SetActive(true);
                //find out where the plane can fit into existing planes
                spawnPos = hitInfo.point;
                spawnPos.x = Mathf.Round(spawnPos.x / 2f) * 2;
                spawnPos.z = Mathf.Round(spawnPos.z / 2f) * 2;
                objToSpawn.transform.position = spawnPos;
            }
            else
            {
                objToSpawn.SetActive(false);
            }
            yield return null;
        }

        if (spawnUnitNow)
        {
            Spawn(HousePrefab, spawnPos);
        }
        GameObject.Destroy(objToSpawn);
        spawnUnitNow = false;
        yield return null;
    }

    void Spawn(GameObject prefabToSpawn, Vector3 spawnPos)
    {
        lastRotation = objToSpawn.transform.rotation;
        GameObject go = GameObject.Instantiate(prefabToSpawn, spawnPos, objToSpawn.transform.rotation);
        SpawnableBase[] obs = go.GetComponents<SpawnableBase>();
        foreach (SpawnableBase sb in obs)
            sb.Spawn();
        //TODO: Factory would help here
    }
}
