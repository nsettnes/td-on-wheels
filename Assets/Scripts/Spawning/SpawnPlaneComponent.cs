﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class SpawnPlaneComponent : SpawnableBase
{
    public override void Spawn()
    {
        NavPlaneController.Instance.AddNavPlane(this.transform);
        NavigationBaker.Instance.Bake();
    }

    private void OnEnable()
    {

    }
}
