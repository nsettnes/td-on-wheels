﻿using UnityEngine;

public class DefensiveComponent : MonoBehaviour
{
    [SerializeField]
    private float healthMax;
    public float HealthMax { get { return healthMax; } }
    [SerializeField]
    private float healthCurrent;
    public float HealthCurrent { get { return healthCurrent; } }
    [SerializeField]
    [Tooltip("How many health points can be regenerated per second")]
    private float healthRegen;
    public float HealthRegen { get { return healthRegen; } }

    public void Attack(OffensiveComponent oc)
    {
        healthCurrent -= oc.Damage;
        oc.AttackPerformed();
    }
}