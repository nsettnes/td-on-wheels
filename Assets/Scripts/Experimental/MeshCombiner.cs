﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshCombiner : MonoBehaviour
{
    public List<MeshFilter> meshFilters;
    private CombineInstance[] combines;

    static MeshCombiner _instance;
    public static MeshCombiner Instance
    {
        get
        {
            if (_instance == null)
                _instance = (MeshCombiner)GameObject.FindObjectOfType(typeof(MeshCombiner));

            return _instance;
        }
    }

    private void Start()
    {
        SpawnPlaneComponent[] planers = GameObject.FindObjectsOfType<SpawnPlaneComponent>();
        foreach (SpawnPlaneComponent sp in planers)
            meshFilters.Add(sp.transform.GetComponent<MeshFilter>());
        CombineAll();
    }

    public void CombineMeshFilter(MeshFilter newfilter)
    {
        meshFilters = new List<MeshFilter>();
        meshFilters.Add(this.GetComponent<MeshFilter>());
        meshFilters.Add(newfilter);
        CombineAll();
    }

    void CombineAll ()
    {
        combines = new CombineInstance[meshFilters.Count];
        int i = 0;
        while (i < meshFilters.Count)
        {
            combines[i].mesh = meshFilters[i].sharedMesh;
            combines[i].transform = meshFilters[i].transform.localToWorldMatrix;
            if(!meshFilters[i].Equals(this.GetComponent<MeshFilter>()))
                meshFilters[i].gameObject.SetActive(false);
            i++;
        }
        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combines);
        AutoWeld.Weld(transform.GetComponent<MeshFilter>().mesh);
    }
}
