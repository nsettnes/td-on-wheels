﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionRingAnimator : MonoBehaviour
{
    private Projector projector;
    public float fovFrom = 50, fovTo = 75, prSec = 0.5f;

    float helperF = 0;
    private void Start()
    {
        projector = transform.GetComponentInChildren<Projector>();
    }
    void Update ()
    {
        helperF = Mathf.Sin(Time.time * Mathf.PI / (1f/prSec));
        projector.fieldOfView = fovFrom + (helperF * (fovTo - fovFrom));
	}
}
