﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//intended as the parent for all selectable units friendly and enemies
[RequireComponent(typeof(UnitSelectVisuals))]
public abstract class Unit : MonoBehaviour
{
    public Vector3 WorldPosition { get { return transform.position; } }

    protected UnitSelectVisuals selectVisuals;

    public bool isSelected { get; private set; }


    void Awake()
    {
        Debug.Log("Awake from Unit");
        selectVisuals = this.GetComponent<UnitSelectVisuals>();
        SelfAwake_Unit();
    }

    void Start()
    {
        Debug.Log("Start from Unit");
        SelfStart_Unit();
    }

    /// <summary>
    /// Meant to be overridden in descendants, as a replacement for Awake()
    /// </summary>
    protected virtual void SelfAwake_Unit() { }

    /// <summary>
    /// Meant to be overridden in descendants, as a replacement for Start()
    /// </summary>
    protected virtual void SelfStart_Unit() { }

    public virtual void Deselect()
    {
        isSelected = false;
        selectVisuals.Deselect();
    }

    public virtual void Select()
    {
        isSelected = true;
        selectVisuals.Select();
    }
}
