﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    #region singleton
    static UnitController _instance;
    public static UnitController Instance
    {
        get
        {
            if (_instance == null)
                _instance = (UnitController)GameObject.FindObjectOfType(typeof(UnitController));

            return _instance;
        }
    }
    #endregion singleton

    public GameObject corpsePrefab;

    List<FriendlyUnit> friendlyUnits = new List<FriendlyUnit>();
    List<EnemyUnit> enemyUnits = new List<EnemyUnit>();
    List<Unit> allUnits = new List<Unit>();

    private List<HouseUnit> houseUnits = new List<HouseUnit>();
    public List<HouseUnit> HouseUnits
    {
        get { return houseUnits; }
    }

    public bool isSelectionFriendly { get; private set; }
    public bool hasSelection { get; private set; }
    static List<FriendlyUnit> selectedFriendlyUnits = new List<FriendlyUnit>();
    public List<FriendlyUnit> SelectedFriendlyUnits { get { return selectedFriendlyUnits; } }
    static List<EnemyUnit> selectedEnemyUnits = new List<EnemyUnit>();
    public List<EnemyUnit> SelectedEnemyUnits { get { return selectedEnemyUnits; } }

    private void Start()
    {
        Unit[] units = GameObject.FindObjectsOfType<Unit>();
        foreach(Unit unit in units)
        {
            System.Type t = unit.GetType();
            if(t == typeof(FriendlyUnit))
            {
                //these add themselves through Awake() (should prolly be done in a factory later)
                //friendlyUnits.Add((FriendlyUnit)u);
                //allUnits.Add(u);
            }
            else if(t == typeof(EnemyUnit))
            {
                //TODO: these should also add themselves in Awake() when they are spawned
                AddNewMovingUnit((MovingUnit)unit);
            }
        }

        //HouseUnit[] hunits = GameObject.FindObjectsOfType<HouseUnit>();
        //foreach (HouseUnit hu in hunits)
        //{
        //    HouseUnits.Add(hu);
        //}
    }

    public void ClearSelection()
    {
        foreach (FriendlyUnit fu in selectedFriendlyUnits)
        {
            fu.Deselect();
        }

        selectedFriendlyUnits.Clear();
        foreach (EnemyUnit eu in selectedEnemyUnits)
        {
            eu.Deselect();
        }
        selectedEnemyUnits.Clear();

        hasSelection = false;
    }

    public void TrySelectUnitsInsideBounds(Bounds bounds)
    {
        ClearSelection();

        //you might want to decide how to go about selection of house units. House units should prolly be single-click only
        foreach(FriendlyUnit fu in friendlyUnits)
        {
            if(IsWithinSelectionBounds(fu.WorldPosition, bounds))
            {
                fu.Select();
                selectedFriendlyUnits.Add(fu);
            }
        }

        if(selectedFriendlyUnits.Count == 0)
        {
            foreach (EnemyUnit eu in enemyUnits)
            {
                if (IsWithinSelectionBounds(eu.WorldPosition, bounds))
                {
                    eu.Select();
                    selectedEnemyUnits.Add(eu);
                }
            }
        }
        hasSelection = selectedFriendlyUnits.Count > 0 || selectedEnemyUnits.Count > 0;
        isSelectionFriendly = selectedFriendlyUnits.Count > 0;
    }

    public bool IsWithinSelectionBounds(Vector3 worldPosition, Bounds bounds)
    {
        Camera camera = Camera.main;
        return bounds.Contains(camera.WorldToViewportPoint(worldPosition));
    }

    public void AddNewMovingUnit(MovingUnit unit)
    {
        allUnits.Add(unit);
        if (unit.GetType() == typeof(FriendlyUnit))
        {
            friendlyUnits.Add((FriendlyUnit)unit);
        }
        else if (unit.GetType() == typeof(EnemyUnit))
        {
            enemyUnits.Add((EnemyUnit)unit);
        }

        unit.UnitDied += UnitDiedHandler;
    }

    private void UnitDiedHandler(MovingUnit unit)
    {
        
        if(unit.GetType() == typeof(FriendlyUnit))
        {
            friendlyUnits.Remove((FriendlyUnit)unit);
            selectedFriendlyUnits.Remove((FriendlyUnit)unit);
        }
        else if (unit.GetType() == typeof(EnemyUnit))
        {
            enemyUnits.Remove((EnemyUnit)unit);
            selectedEnemyUnits.Remove((EnemyUnit)unit);
        }
        allUnits.Remove(unit);

        MovingUnitDied_handler(unit);
    }

    void MovingUnitDied_handler(MovingUnit mu)
    {
        GameObject.Instantiate(corpsePrefab, mu.transform.position, corpsePrefab.transform.rotation);
        Destroy(mu.gameObject);
    }

    #region enemy units
    /// <summary>
    /// Gets a list of enemy units in range of a given unit
    /// </summary>
    /// <param name="_unit">Origin unit</param>
    /// <param name="range">Range within which enemies will be included</param>
    /// <param name="bufferRange">Extra range on top of range - this is used for when you want a unit to be included if its -edge- is within range but not its center</param>
    /// <returns></returns>
    public List<EnemyUnit> GetEnemiesInRange(Unit _unit, float range, float bufferRange = 1f)
    {
        return enemyUnits.Where(x => DistanceBetweenUnits(_unit, x) < range + bufferRange).ToList();
    }

    public float DistanceBetweenUnits(Unit u1, Unit u2)
    {
        return (u1.WorldPosition - u2.WorldPosition).magnitude;
    }
    #endregion enemy units

    #region house units
    public void AddNewHouseUnit(HouseUnit hu)
    {
        HouseUnits.Add(hu);
    }

    public HouseUnit GetClosestHouseUnit(Vector3 point)
    {
        HouseUnit res = null;
        float smallestDistance = float.MaxValue;
        foreach(HouseUnit hu in HouseUnits)
        {
            if ((hu.WorldPosition - point).magnitude < smallestDistance)
            {
                res = hu;
                smallestDistance = (hu.WorldPosition - point).magnitude;
            }
        }
        return res;
    }
    #endregion house units
}
