﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpseAnimation : MonoBehaviour
{
    public GameObject deleteWhenDone;
    public float totalTime;
    public AnimationCurve animCurve;
    public Vector3 endSize;
    public Vector3 endPositionRelative;

    private Vector3 startSize;
    private Vector3 startPosition;

    private float _time = 0;
    private float _ratio = 0;
    private void Start()
    {
        startSize = transform.localScale;
        startPosition = transform.position;
        _time = totalTime;
    }

    void Update ()
    {
		if(_time > 0)
        {
            _time -= Time.deltaTime;
            _ratio = animCurve.Evaluate(1 - _time / totalTime);
            transform.localScale = endSize * _ratio + startSize * (1 - _ratio);
            transform.position = startPosition + _ratio * endPositionRelative;
        }
        else
        {
            Destroy(this.gameObject);
        }
	}
}
