﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIS_UnitSelecting : IUserInteractionState
{
    public override event UserInteraction.StateWantsToExit WantToExit;

    public UIS_UnitSelecting()
    {

    }

    public override void Enter()
    {
        UnitSelectionComponent.Instance.BeginSelect();
        //throw new System.NotImplementedException();
    }

    public override void Exit()
    {
        UnitSelectionComponent.Instance.CancelSelect();
        //throw new System.NotImplementedException();
    }

    public override void HandleInput(InputType itype)
    {
        if(itype.IsDown & itype.keyCode == KeyCode.Mouse0)
        {
            
        }
        else if(itype.IsUp & itype.keyCode == KeyCode.Mouse0)
        {
            UnitSelectionComponent.Instance.EndSelect();
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_UnitSelection()); });
        }
        else if (itype.IsDown & itype.keyCode == KeyCode.Mouse1)
        {
            UnitSelectionComponent.Instance.CancelSelect();
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_Idle()); });
                
        }
        if (itype.IsUp & UserInteraction.Instance.keycodes_enter_spawn.Contains(itype.keyCode))
        {
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_Spawn(itype.keyCode, KeyCode.Mouse0, KeyCode.R, new KeyCode[] { KeyCode.Mouse1, KeyCode.Escape })); });
            else
                Debug.Log("UIS_Idle tried to exit to state UIS_Spawn, but WantToExit is null");
        }
    }
}
