﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIS_Spawn : IUserInteractionState
{
    public override event UserInteraction.StateWantsToExit WantToExit;

    public enum SPAWNTYPE { Obstruction, Plane, House }
    private KeyCode keyTrigger;
    private KeyCode keyEndSpawn;
    private KeyCode keyRotateSpawn;
    private List<KeyCode> keysCancelSpawn;

    private Dictionary<KeyCode, SPAWNTYPE> keyToSpawnType = new Dictionary<KeyCode, SPAWNTYPE>()
    {
        { KeyCode.Alpha1, SPAWNTYPE.Obstruction },
        { KeyCode.Alpha2, SPAWNTYPE.Plane },
        { KeyCode.Alpha3, SPAWNTYPE.House }
    };

    public UIS_Spawn(KeyCode _triggerKey, KeyCode _keyEndSpawn, KeyCode _keyRotateSpawn, KeyCode[] _keysCancelSpawn)
    {
        keyTrigger = _triggerKey;
        keyEndSpawn = _keyEndSpawn;
        keyRotateSpawn = _keyRotateSpawn;
        keysCancelSpawn = new List<KeyCode>();
        keysCancelSpawn.AddRange(_keysCancelSpawn);
    }


    public override void Enter()
    {
        Spawner.Instance.BeginSpawn(keyToSpawnType[keyTrigger]);
    }

    public override void Exit()
    {
        Spawner.Instance.CancelSpawn();
    }

    public override void HandleInput(InputType itype)
    {
        if (itype.keyCode == keyEndSpawn && itype.IsUp)
        {
            if (WantToExit != null)
            {
                Spawner.Instance.EndSpawn();
                DelayedExit(() => { WantToExit(new UIS_Idle()); });
            }
        }
        else if (itype.keyCode == keyRotateSpawn && itype.IsDown)
        {
            Spawner.Instance.RotateSpawn();
        }
        else if (keysCancelSpawn.Contains(itype.keyCode) && itype.IsUp)
        {
            Spawner.Instance.CancelSpawn();
            if (WantToExit != null)
            {
                DelayedExit(() => { WantToExit(new UIS_Idle()); });
            }
        }
    }
}
