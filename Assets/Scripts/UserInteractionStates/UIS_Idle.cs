﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIS_Idle : IUserInteractionState
{
    public override event UserInteraction.StateWantsToExit WantToExit;

    public override void Enter()
    {
        //throw new System.NotImplementedException();
    }

    public override void Exit()
    {
        //throw new System.NotImplementedException();
    }

    public override void HandleInput(InputType itype)
    {
        if(itype.IsUp & UserInteraction.Instance.keycodes_enter_spawn.Contains(itype.keyCode))
        {
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_Spawn(itype.keyCode, KeyCode.Mouse0, KeyCode.R, new KeyCode[] { KeyCode.Mouse1, KeyCode.Escape })); });
            else
                Debug.Log("UIS_Idle tried to exit to state UIS_Spawn, but WantToExit is null");
        }
        else if(itype.IsDown & itype.keyCode == KeyCode.Mouse0)
        {
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_UnitSelecting()); });
            else
                Debug.Log("UIS_Idle tried to exit to state UIS_UnitSelect, but WantToExit is null");
        }
    }
}
