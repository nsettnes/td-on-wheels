﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIS_UnitSelection : IUserInteractionState
{
    public override event UserInteraction.StateWantsToExit WantToExit;

    public UIS_UnitSelection()
    {

    }

    public override void Enter()
    {
        if(UnitController.Instance.SelectedFriendlyUnits.Count == 0)
        {
            if (WantToExit != null)
                WantToExit(new UIS_Idle());
        }
        //throw new System.NotImplementedException();
    }

    public override void Exit()
    {
        //throw new System.NotImplementedException();
    }

    public override void HandleInput(InputType itype)
    {
        if (itype.IsUp & UserInteraction.Instance.keycodes_enter_spawn.Contains(itype.keyCode))
        {
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_Spawn(itype.keyCode, KeyCode.Mouse0, KeyCode.R, new KeyCode[] { KeyCode.Mouse1, KeyCode.Escape })); });
            else
                Debug.Log("UIS_Idle tried to exit to state UIS_Spawn, but WantToExit is null");
        }
        else if (itype.IsDown & itype.keyCode == KeyCode.Mouse1)
        {
            Ray ray;
            RaycastHit hitInfo;

            ray = Camera.main.ScreenPointToRay(itype.mousePoint);
            if(Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("House")))
            {
                HouseUnit hu = UnitController.Instance.GetClosestHouseUnit(hitInfo.point);
                UnitController.Instance.SelectedFriendlyUnits.ForEach(x => x.ActionMoveIntoHouse(hu));
            }
            else if (Physics.Raycast(ray, out hitInfo, 1000f, 1 << LayerMask.NameToLayer("Walkable")))
            {
                //general direction of all selected units towards to hitpoint
                Vector3 dirForward = Vector3.zero;
                UnitController.Instance.SelectedFriendlyUnits.ForEach(x => dirForward += (hitInfo.point - x.WorldPosition));
                dirForward = dirForward.normalized;
                Vector3 dirLeft = Vector3.Cross(dirForward, Vector3.up).normalized;

                //if more units are selected: get positions in a line with the hitpoint being the midpoint
                if (UnitController.Instance.SelectedFriendlyUnits.Count > 1)
                {
                    float startCount = ((float)UnitController.Instance.SelectedFriendlyUnits.Count * 1.5f - 1f) / 2f;
                    int index = 0;
                    for(float i = -startCount; i <= startCount; i += 1.5f)
                    {
                        UnitController.Instance.SelectedFriendlyUnits[index].ActionMove(hitInfo.point + dirLeft * i);
                        index++;
                    }
                }
                else if(UnitController.Instance.SelectedFriendlyUnits.Count == 1)
                {
                    UnitController.Instance.SelectedFriendlyUnits[0].ActionMove(hitInfo.point);
                }
            }
        }
        else if(itype.IsDown & itype.keyCode == KeyCode.Mouse0)
        {
            if (WantToExit != null)
                DelayedExit(() => { WantToExit(new UIS_UnitSelecting());  });
        }
    }
}
