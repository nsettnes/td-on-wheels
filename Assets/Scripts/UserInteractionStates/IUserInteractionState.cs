﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//not an interface because I want to declare an ExitStateDelegate
public abstract class IUserInteractionState
{
    //if this is not here, we cant reference the "new" WantToExits in the subclasses
    public abstract event UserInteraction.StateWantsToExit WantToExit;
    public abstract void Enter();
    public abstract void HandleInput(InputType itype);
    public abstract void Exit();

    protected delegate void ExitStateDelegate();

    protected void DelayedExit(ExitStateDelegate myExitDelegate)
    {
        if(cr_exitstate == null)
        {
            cr_exitstate = ExitState(myExitDelegate);
            UserInteraction.Instance.StartCoroutine(cr_exitstate);
        }
    }

    IEnumerator cr_exitstate;
    /// <summary>
    /// Used for methods to do delayed exiting, for instance when a coroutine needs to 
    /// finish up before closing the state down.
    /// </summary>
    /// <returns></returns>
    protected IEnumerator ExitState(ExitStateDelegate myExitDelegate)
    {
        yield return null;
        if (myExitDelegate != null)
            myExitDelegate();
        yield return null;
    }
}
