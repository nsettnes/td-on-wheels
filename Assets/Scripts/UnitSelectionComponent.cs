﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectionComponent : MonoBehaviour
{
    #region singleton
    static UnitSelectionComponent _instance;
    public static UnitSelectionComponent Instance
    {
        get
        {
            if (_instance == null)
                _instance = (UnitSelectionComponent)GameObject.FindObjectOfType(typeof(UnitSelectionComponent));

            return _instance;
        }
    }
    #endregion singleton

    bool isSelecting = false;
    Vector3 mousePosition1;
    Vector3 mousePosition2;

    public void BeginSelect()
    {
        Debug.Log("BeginSelecting");
        isSelecting = true;
        mousePosition1 = Input.mousePosition;
    }

    public void EndSelect()
    {
        Debug.Log("EndSelecting");
        DoSelection(Utils.GetViewportBounds(Camera.main, mousePosition1, mousePosition2));
        isSelecting = false;
    }

    public void CancelSelect()
    {
        isSelecting = false;
    }

    void Update()
    {
        if(isSelecting)
        {
            mousePosition2 = Input.mousePosition;
        }
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect(mousePosition1, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    void DoSelection(Bounds bounds)
    {
        UnitController.Instance.TrySelectUnitsInsideBounds(bounds);
    }
}