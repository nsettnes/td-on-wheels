﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FriendlyUnit : MovingUnit
{
    protected HouseUnit HouseUnitTarget;
    protected HouseUnit CurrentHouseUnit;
    bool isInHouse = false;

    public bool isInitialized { get; private set; }

    public override void Deselect()
    {
        base.Deselect();
    }

    protected override void SelfAwake_MovingUnit()
    {
        if (!isInitialized)
        {
            navLogic.OnNavMeshLink += NavLogic_OnNavMeshLink;
            UnitController.Instance.AddNewMovingUnit(this);
            isInitialized = true;
            navLogic.OnDestinationUnreachable += NavLogic_OnDestinationUnreachable;
        }
    }

    protected override void SelfStart_MovingUnit()
    {
        //Debug.Log("SelfStart_MovingUnit from Friend");
    }

    private void NavLogic_OnDestinationUnreachable()
    {
        //Debug.Log("Destination became unreachable!");
    }

    private void NavLogic_OnNavMeshLink(bool isOnLink)
    {
        if(isOnLink)
        {
            HouseUnit hu = navLogic.GetHouseUnitOfCurrentOffMeshLink();
            if (hu == null)
                return; 

            if (HouseUnitTarget == hu)
            {
                HouseUnitTarget.Enter(this);
                CurrentHouseUnit = HouseUnitTarget;
                isInHouse = true;
            }
            else if (CurrentHouseUnit == hu)
            {
                CurrentHouseUnit.Exit(this);
                CurrentHouseUnit = null;
                isInHouse = false;
            }
        }
        else // !isOnLink
        {
            if (navLogic.HouseUnitOfLastOffMeshLink == null)
                return;

            if (HouseUnitTarget == navLogic.HouseUnitOfLastOffMeshLink)
            {
                //HouseUnitTarget.Enter(this);
                //CurrentHouseUnit = HouseUnitTarget;
                //isInHouse = true;
            }
            else if (CurrentHouseUnit == navLogic.HouseUnitOfLastOffMeshLink)
            {
                //CurrentHouseUnit.Exit(this);
                //CurrentHouseUnit = null;
                //isInHouse = false;
            }
        }
    }

    public override void Select()
    {
        base.Select();
    }

    public void ActionMove(Vector3 moveToPoint)
    {
        HouseUnitTarget = null;
        navLogic.SetDestinationAsPoint(moveToPoint);
    }

    public void ActionMoveIntoHouse(HouseUnit hu)
    {
        HouseUnitTarget = hu;
        Vector3 newDest = hu.WorldPosition;
        newDest.y = 0;
        navLogic.SetDestinationAsPoint(newDest);
    }
}
