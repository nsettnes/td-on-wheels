﻿using UnityEngine;

public class OffensiveComponent : MonoBehaviour
{
    [SerializeField]
    private float range;
    public float Range { get { return range; } }
    [SerializeField]
    private float damage;
    public float Damage { get { return damage; } }

    [SerializeField]
    private float timeBetweenAttacks;
    public float TimeBetweenAttacks { get { return timeBetweenAttacks; } }
    [SerializeField]
    private float lastAttackTimestamp;
    public float LastAttackTimestamp { get { return lastAttackTimestamp; } }
    public bool CanAttackNow { get { return Time.time - LastAttackTimestamp >= timeBetweenAttacks; } }

    public void AttackPerformed()
    {
        lastAttackTimestamp = Time.time;
    }
}