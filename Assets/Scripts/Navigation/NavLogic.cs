﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public class NavLogic : MonoBehaviour
{
    public event Action OnDestinationUnreachable;

    public Transform targetTransform;
    public Vector3 destinationPoint = Vector3.zero;
    public NavMeshAgent Agent { get; private set; }

    public HouseUnit HouseUnitOfLastOffMeshLink { get; private set; }

    bool destinationUnreachable = false;
    bool destinationReached = false;
    bool onNavMeshLink = false;

    public event Action<bool> OnNavMeshLink;
    public event Action OnDestinationReached;

    public bool IsEnabled { get; private set; }

    //temp variables stored for better memory performance
    NavMeshPath tempPath;

    private void Awake()
    {
        IsEnabled = true;
        Agent = this.GetComponent<NavMeshAgent>();
        destinationPoint = transform.position;
    }

    void Start()
    {
        NavigationBaker.Instance.OnBake += Instance_OnBake;
    }

    void Update()
    {
        if(IsEnabled)
        {
            UpdateOffMeshLinkStatus();
            UpdateDestinationStatus();
        }
    }

    private void UpdateOffMeshLinkStatus()
    {
        if (onNavMeshLink != Agent.isOnOffMeshLink)
        {
            onNavMeshLink = !onNavMeshLink;
            if (OnNavMeshLink != null)
                OnNavMeshLink(onNavMeshLink);
            if (onNavMeshLink)
                HouseUnitOfLastOffMeshLink = GetHouseUnitOfCurrentOffMeshLink();
        }
    }

    private void UpdateDestinationStatus()
    {
        if (Agent.isOnNavMesh || Agent.isOnOffMeshLink)
        {
            if (!destinationUnreachable && !destinationReached && Agent.remainingDistance < 0.5f)
            {
                destinationReached = true;
                Agent.isStopped = true;
                if (OnDestinationReached != null)
                    OnDestinationReached();
            }

            tempPath = new NavMeshPath();
            if ((targetTransform == null) && (destinationPoint != transform.position) && !Agent.CalculatePath(destinationPoint, tempPath))
            {
                Agent.isStopped = true;
                destinationUnreachable = true;
                if (OnDestinationUnreachable != null)
                    OnDestinationUnreachable();
            }
            else if (targetTransform != null && !Agent.CalculatePath(targetTransform.position, tempPath))
            {
                Agent.isStopped = true;
                destinationUnreachable = true;
                if (OnDestinationUnreachable != null)
                    OnDestinationUnreachable();
            }
        }
    }

    private void ResetMovement()
    {
        Agent.isStopped = false;
        destinationUnreachable = false;
        destinationReached = false;
        targetTransform = null;
        destinationPoint = transform.position;
    }

    private void Instance_OnBake()
    {
        SetTargetTransformAsDestination(targetTransform);
    }

    public void SetTargetTransformAsDestination(Transform newTarget)
    {
        ResetMovement();

        targetTransform = newTarget;
        if (targetTransform != null)
            Agent.SetDestination(targetTransform.position);
    }

    public void SetDestinationAsPoint(Vector3 point)
    {
        ResetMovement();

        destinationPoint = point;
        Agent.SetDestination(destinationPoint);
    }

    public HouseUnit GetHouseUnitOfCurrentOffMeshLink()
    {
        try
        {
            HouseUnit hu = Agent.currentOffMeshLinkData.offMeshLink.transform.parent.GetComponent<HouseUnit>();
            return hu;
        }
        catch (Exception e)
        {
            Debug.LogError("Tried to get HouseUnit from navLogic.Agent.currentOffMeshLinkData.offMeshLink, but failed: " + e.Message);
        }
        return null;
    }

    public void SetEnabled(bool _enable)
    {
        IsEnabled = _enable;
        ResetMovement();
        Agent.isStopped = true;
        Agent.velocity = Vector3.zero;
    }
}
