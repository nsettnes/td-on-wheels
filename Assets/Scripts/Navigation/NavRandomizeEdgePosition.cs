﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavRandomizeEdgePosition : MonoBehaviour
{
    public float repositionRate = 3;

    NavMeshHit hit;
    Vector3 finalPosition;

    void Start()
    {
        Invoke("RandomizePosition", repositionRate);
    }

    void RandomizePosition()
    {
        //find extents of navplanes
        Vector4 extends = NavPlaneController.Instance.GetNavPlaneExtends();

        float dimX = 0, dimZ = 0;
        dimX = extends.y - extends.x;
        dimZ = extends.w - extends.z;

        Vector3 randPos = new Vector3(extends.x + Random.value * dimX, 0, extends.z + Random.value * dimZ);

        //find closest point on actual navmesh (sometimes our point is in a hole of the navmesh)
        NavMesh.SamplePosition(randPos, out hit, 100, 1);
        randPos = hit.position;

        NavMesh.FindClosestEdge(randPos, out hit, 1);
        finalPosition = hit.position;
        finalPosition.y = transform.position.y;
        transform.position = finalPosition;

        Invoke("RandomizePosition", repositionRate);
    }
}
