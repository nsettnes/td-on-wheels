﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavPlaneController : MonoBehaviour {
    static NavPlaneController _instance;
    public static NavPlaneController Instance
    {
        get
        {
            if (_instance == null)
                _instance = (NavPlaneController)GameObject.FindObjectOfType(typeof(NavPlaneController));

            return _instance;
        }
    }

    public List<Transform> navPlanes = new List<Transform>();

    private void Start()
    {
        SpawnPlaneComponent[] objs = GameObject.FindObjectsOfType<SpawnPlaneComponent>();
        foreach (SpawnPlaneComponent sp in objs)
            navPlanes.Add(sp.transform);
    }

    public void AddNavPlane(Transform trans)
    {
        navPlanes.Add(trans);
    }

    public Vector4 GetNavPlaneExtends()
    {
        float minx = 0, maxx = 0, minz = 0, maxz = 0;

        for (int i = 0; i < Instance.navPlanes.Count; i++)
        {
            if (Instance.navPlanes[i].position.x < minx)
                minx = Instance.navPlanes[i].position.x;
            if (Instance.navPlanes[i].position.x > maxx)
                maxx = Instance.navPlanes[i].position.x;
            if (Instance.navPlanes[i].position.z < minz)
                minz = Instance.navPlanes[i].position.z;
            if (Instance.navPlanes[i].position.z > maxz)
                maxz = Instance.navPlanes[i].position.z;
        }
        return new Vector4(minx, maxx, minz, maxz);
    }
}
