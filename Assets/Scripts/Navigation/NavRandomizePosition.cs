﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavRandomizePosition : MonoBehaviour
{
    public float maxDistance = 10;
    public float repositionRate = 3;

    NavMeshHit hit;
    Vector3 randomDirection;
    Vector3 finalPosition;

    void Start()
    {
        Invoke("RandomizePosition", repositionRate);
	}

    void RandomizePosition()
    {
        randomDirection = Random.insideUnitCircle * maxDistance;
        randomDirection.z = randomDirection.y;
        randomDirection.y = 0;
        //randomDirection += transform.position;
        NavMesh.SamplePosition(randomDirection, out hit, maxDistance, 1);
        finalPosition = hit.position;
        finalPosition.y = transform.position.y;
        transform.position = finalPosition;



        Invoke("RandomizePosition", repositionRate);
    }
}
