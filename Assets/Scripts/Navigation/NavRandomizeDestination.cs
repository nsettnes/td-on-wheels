﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavLogic))]
public class NavRandomizeDestination : MonoBehaviour
{
    public float maxDistance = 10;
    public float searchForNavMeshHitThisFar = 4;
    public float waitAtDestination = 3;
    public float randomWaitFactor = 2;
    public float goalDistanceFromDestination = 0.5f;

    NavMeshHit hit;
    Vector3 randomDirection;
    Vector3 finalPosition;

    Vector3 currentDestination;
    Vector3 dest1, dest2;

    NavLogic navLogic;


    bool IsMyUnitAlive
    {
        get
        {
            MovingUnit myUnit = this.GetComponent<MovingUnit>();
            return myUnit != null && myUnit.IsAlive;
        }
    }

    void Start()
    {
        navLogic = GetComponent<NavLogic>();
        Invoke("NewDestination", 0.5f);
    }

    void NewDestination()
    {
        if (cr_randomizeposition != null)
            StopCoroutine(cr_randomizeposition);

        if (!IsMyUnitAlive)
            return; 

        cr_randomizeposition = RandomizePosition();
        StartCoroutine(RandomizePosition());
    }

    IEnumerator cr_randomizeposition;
    IEnumerator RandomizePosition()
    {
        bool hitFound = false;
        do
        {
            //find new destination
            randomDirection = Random.insideUnitCircle * maxDistance;
            randomDirection.z = randomDirection.y;
            randomDirection.y = 0;
            int escapeInfiniteLoop = 0;
            while(!hitFound && escapeInfiniteLoop < 5)
            {
                escapeInfiniteLoop++;
                hitFound = NavMesh.SamplePosition(randomDirection + transform.position, out hit, searchForNavMeshHitThisFar, 1);
            }

            finalPosition = hit.position;
            finalPosition.y = transform.position.y;
            currentDestination = finalPosition;
            if (!hitFound)
                yield return null;
        }
        while (!hitFound);

        if (!IsMyUnitAlive)
            StopCoroutine(cr_randomizeposition);

        navLogic.SetDestinationAsPoint(finalPosition);

        //wait for goal distance from destination to be reached
        bool destinationReached = false;
        bool destinationReachable = false;
        while (!destinationReached && destinationReachable)
        {
            dest1 = transform.position;
            dest1.y = 0;
            dest2 = currentDestination;
            dest2.y = 0;
            destinationReached = (dest1 - dest2).magnitude > goalDistanceFromDestination;
            destinationReachable = NavMesh.SamplePosition(currentDestination, out hit, 0.1f, 1);
            yield return null;
        }

        //wait a while at the destination before moving on
        float timer = waitAtDestination + randomWaitFactor * waitAtDestination * Random.value;
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        yield return null;

        NewDestination();
    }

}
