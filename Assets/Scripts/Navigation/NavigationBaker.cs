﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class NavigationBaker : MonoBehaviour
{
    public delegate void BakeHandler();
    public event BakeHandler OnBake;

    public List<NavMeshSurface> surfaces;

    bool flagBake = false;

    static NavigationBaker _instance; 
    public static NavigationBaker Instance
    {
        get
        {
            if (_instance == null)
                _instance = (NavigationBaker)GameObject.FindObjectOfType(typeof(NavigationBaker));

            return _instance;
        }
    }

    private void Awake()
    {
        flagBake = true;
    }

    public void AddNavSurface(NavMeshSurface navsurf)
    {
        surfaces.Add(navsurf);
    }

    public void Bake()
    {
        for (int i = 0; i < surfaces.Count; i++)
        {
            surfaces[i].RemoveData();
            surfaces[i].BuildNavMesh();
        }

        if (OnBake != null)
            OnBake();
    }

    private void LateUpdate()
    {
        if(flagBake)
        {
            Bake();
            flagBake = false;
        }
    }
}