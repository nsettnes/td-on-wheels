﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInteraction : MonoBehaviour
{
    #region singleton
    static UserInteraction _instance;
    public static UserInteraction Instance
    {
        get
        {
            if (_instance == null)
                _instance = (UserInteraction)GameObject.FindObjectOfType(typeof(UserInteraction));

            return _instance;
        }
    }
    #endregion singleton

    public delegate void StateWantsToExit(IUserInteractionState nextState);

    public List<KeyCode> keycodes_enter_spawn = new List<KeyCode>()
    {
        KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3
    };

    private IUserInteractionState currentUIS;

    void OnGUI()
    {
        //if(currentUIS.GetType() == typeof(UIS_Idle))
            HandleCurrentEvent();
    }

    void HandleCurrentEvent()
    {
        Event e = Event.current;

        if (e.isKey)
        {
            //apparently, every other keypress comes as a KeyCode.None, with only the "character" set
            if (e.keyCode == KeyCode.None)
                return;

            InputType itype = new InputType(e.keyCode, InputType.INPUT.Held, Vector3.zero);
            if (Input.GetKeyDown(e.keyCode))
                itype.input = InputType.INPUT.Down;
            else if (Input.GetKeyUp(e.keyCode))
                itype.input = InputType.INPUT.Up;

            currentUIS.HandleInput(itype);
        }
        else if (e.isMouse)
        {
            InputType itype = new InputType(e.keyCode, InputType.INPUT.Held, Input.mousePosition);
            switch(e.button)
            {
                case 0:
                    itype.keyCode = KeyCode.Mouse0;
                    if (Input.GetKeyDown(KeyCode.Mouse0))
                        itype.input = InputType.INPUT.Down;
                    else if (Input.GetKeyUp(KeyCode.Mouse0))
                        itype.input = InputType.INPUT.Up;
                    break;
                case 1:
                    itype.keyCode = KeyCode.Mouse1;
                    if (Input.GetKeyDown(KeyCode.Mouse1))
                        itype.input = InputType.INPUT.Down;
                    else if(Input.GetKeyUp(KeyCode.Mouse1))
                        itype.input = InputType.INPUT.Up;
                    break;
            }
            currentUIS.HandleInput(itype);
        }
    }

    private void Awake()
    {
        SwitchState(new UIS_Idle());
    }

    void SwitchState(IUserInteractionState newState)
    {
        Debug.Log("Switching state " + newState.GetType().ToString());
        if (currentUIS != null)
        {
            currentUIS.WantToExit -= CurrentUIS_WantToExit;
            currentUIS.Exit();
        }
        currentUIS = newState;
        currentUIS.WantToExit += CurrentUIS_WantToExit;
        currentUIS.Enter();
    }

    private void CurrentUIS_WantToExit(IUserInteractionState nextState)
    {
        SwitchState(nextState);
    }
}
