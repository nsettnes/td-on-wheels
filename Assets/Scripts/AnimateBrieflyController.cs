﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateBrieflyController : MonoBehaviour
{
    #region singleton
    static AnimateBrieflyController _instance;
    public static AnimateBrieflyController Instance
    {
        get
        {
            if (_instance == null)
                _instance = (AnimateBrieflyController)GameObject.FindObjectOfType(typeof(AnimateBrieflyController));

            return _instance;
        }
    }
    #endregion singleton

    [SerializeField]
    private float ShotDisplayTime = 0.5f;
    [SerializeField]
    private Color ShotDisplayColor = Color.red;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DrawShotLine(Vector3 startpoint, Vector3 endpoint)
    {
        Debug.DrawLine(startpoint, endpoint, ShotDisplayColor, ShotDisplayTime);
    }
}
