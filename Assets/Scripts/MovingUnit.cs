﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavLogic))]
[RequireComponent(typeof(DefensiveComponent))]
[RequireComponent(typeof(OffensiveComponent))]
public abstract class MovingUnit : Unit
{
    public event Action<MovingUnit> UnitDied;
    protected NavLogic navLogic;
    public DefensiveComponent defensiveComponent { get; private set; }
    public OffensiveComponent offensiveComponent { get; private set; }
    public bool IsAlive { get; private set; }

    public new Vector3 WorldPosition { get { return navLogic.transform.position; } }


    protected override void SelfAwake_Unit()
    {
        IsAlive = true;
        navLogic = this.GetComponent<NavLogic>();
        defensiveComponent = this.GetComponent<DefensiveComponent>();
        offensiveComponent = this.GetComponent<OffensiveComponent>();

        SelfAwake_MovingUnit();
    }

    protected override void SelfStart_Unit()
    {
        SelfStart_MovingUnit();
    }

    void Update()
    {
        if(IsAlive && defensiveComponent.HealthCurrent <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        IsAlive = false;
        navLogic.SetEnabled(false);
        if (UnitDied != null)
            UnitDied(this);
    }

    /// <summary>
    /// Meant to be overridden in descendants, as a replacement for Awake() -> SelfAwake_Unit()
    /// </summary>
    protected virtual void SelfAwake_MovingUnit() { }

    protected virtual void SelfStart_MovingUnit() { }
}
