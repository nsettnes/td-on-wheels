﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSelectVisuals : MonoBehaviour
{
    public Projector pro; 
    public void Select()
    {
        pro.enabled = true;
    }

    public void Deselect()
    {
        pro.enabled = false;
    }
}
