﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.AI;

public class HouseUnit : Unit
{
    public List<OffMeshLink> entranceLinks;
    public List<OffMeshLink> exitLinks;
    public int UnitCapacity = 3;
    public List<FriendlyUnit> Units { get; private set; }
    private List<UnitRoutineHolder> UnitRoutines = new List<UnitRoutineHolder>();

    protected override void SelfAwake_Unit()
    {
        Debug.Log("SelfAwake_Unit from HouseUnit");
        Units = new List<FriendlyUnit>();
    }

    protected override void SelfStart_Unit()
    {
        Debug.Log("SelfStart_Unit from HouseUnit");
        UnitController.Instance.AddNewHouseUnit(this);
    }

    public bool IsAtFullCapacity
    {
        get { return Units.Count >= UnitCapacity; }
    }

    public void Enter(FriendlyUnit fu)
    {
        Units.Add(fu);
        if (IsAtFullCapacity)
            entranceLinks.ForEach(x => x.activated = false);
        AddUnitRoutine(fu);
    }

    public void Exit(FriendlyUnit fu)
    {
        Units.Remove(fu);
        RemoveUnitRoutine(fu);
        entranceLinks.ForEach(x => x.activated = true);
    }

    void AddUnitRoutine(FriendlyUnit fu)
    {
        UnitRoutines.Add(new UnitRoutineHolder(fu, CR_UnitRoutine(fu), this));
    }

    void RemoveUnitRoutine(FriendlyUnit fu)
    {
        UnitRoutineHolder urh = UnitRoutines.First(x => x.friendlyUnit == fu);
        StopCoroutine(urh.routine);
        UnitRoutines.Remove(urh);
    }

    IEnumerator CR_UnitRoutine(FriendlyUnit fu)
    {
        List<EnemyUnit> enemies;
        EnemyUnit target = null;
        bool targetStillValid = false;
        while(Units.Contains(fu))
        {
            while(!fu.offensiveComponent.CanAttackNow)
            {
                yield return null;
            }

            targetStillValid = target != null && target.IsAlive && UnitController.Instance.DistanceBetweenUnits(this, target) <= fu.offensiveComponent.Range;
            if (!targetStillValid)
            {
                target = null;
                enemies = UnitController.Instance.GetEnemiesInRange(this, fu.offensiveComponent.Range);
                if(!enemies.Contains(target) && enemies.Count > 0)
                {
                    target = enemies.OrderBy(x => Guid.NewGuid()).First();
                }
            }

            if(target != null)
            {
                target.defensiveComponent.Attack(fu.offensiveComponent);
                AnimateBrieflyController.Instance.DrawShotLine(this.transform.position, target.WorldPosition);
            }
            
            yield return null;
        }
        yield return null;
    }


    //public override void Deselect()
    //{
    //    isSelected = false;
    //    selectVisuals.Deselect();
    //}

    //public override void Select()
    //{
    //    isSelected = true;
    //    selectVisuals.Select();
    //}


    class UnitRoutineHolder
    {
        public UnitRoutineHolder(FriendlyUnit _friendlyUnit, IEnumerator _routine, MonoBehaviour invoker)
        {
            routine = _routine;
            friendlyUnit = _friendlyUnit;
            invoker.StartCoroutine(routine);
        }

        public IEnumerator routine;
        public FriendlyUnit friendlyUnit;
    }
}
