﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputType
{
    public enum INPUT { Up, Down, Held }
    public KeyCode keyCode = KeyCode.None;
    public INPUT input = INPUT.Down;
    public Vector3 mousePoint = Vector3.zero;

    public bool IsUp { get { return input == INPUT.Up; } }
    public bool IsDown { get { return input == INPUT.Down; } }
    public bool IsHeld { get { return input == INPUT.Held; } }

    public InputType(KeyCode _keyCode, INPUT _input, Vector3 _mousePoint)
    {
        keyCode = _keyCode;
        input = _input;
        mousePoint = _mousePoint;
    }
}